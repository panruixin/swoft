<p align="center">
    <a href="https://github.com/swoft-cloud/swoft" target="_blank">
        <img src="http://qiniu.daydaygo.top/swoft-logo.png?imageView2/2/w/300" alt="swoft"/>
    </a>
</p>

[![Latest Stable Version](http://img.shields.io/packagist/v/swoft/swoft.svg)](https://packagist.org/packages/swoft/swoft)
[![Build Status](https://travis-ci.org/swoft-cloud/swoft.svg?branch=master)](https://travis-ci.org/swoft-cloud/swoft)
[![Docker Build Status](https://img.shields.io/docker/build/swoft/swoft.svg)](https://hub.docker.com/r/swoft/swoft/)
[![Php Version](https://img.shields.io/badge/php-%3E=7.1-brightgreen.svg?maxAge=2592000)](https://secure.php.net/)
[![Swoole Version](https://img.shields.io/badge/swoole-%3E=4.3.3-brightgreen.svg?maxAge=2592000)](https://github.com/swoole/swoole-src)
[![Swoft Doc](https://img.shields.io/badge/docs-passing-green.svg?maxAge=2592000)](https://www.swoft.org)
[![Swoft License](https://img.shields.io/hexpm/l/plug.svg?maxAge=2592000)](https://github.com/swoft-cloud/swoft/blob/master/LICENSE)
[![Gitter](https://img.shields.io/gitter/room/swoft-cloud/swoft.svg)](https://gitter.im/swoft-cloud/community)

![start-http-server](https://www.swoft.org/img/icon.png)

PHP 高性能微服务协程框架

> **[EN README](README.md)**

## Composer

> **必须安装的**

安装PHP并且版本至少 >7.1，推荐使用 7.2+

安装php包管理器 composer

连接迭代器依赖 pcre 库

安装php扩展swoole, 并且版本至少 >4.3.0，推荐使用最新稳定版本

其他需要安装和启用的php扩展有：PDO redis
- composer create-project swoft/swoft swoft
- 使用 composer 安装 composer require swoft/devtool 插件（用来生成表）
## 有冲突的PHP扩展
下面列出一些已知的和swoole有冲突的php扩展，请使用swoft时不要安装或禁用它们：

- xdebug
- xhprof
- blackfire
- zend
- trace
- uopz

linux下可以用yum -y removeauto xdebug 删除扩展

## HTTP服务

php ./bin/swoft http:start 开启http服务

php ./bin/swoft http:start -d 开启http服务并在后台运行

## 数据库操作

php ./bin/swoft migrate:create User 生成user表

php ./bin/swoft migrate:up 运行所有未运行过的迁移

php ./bin/swoft migrate:down 将会回滚迁移

php ./bin/swoft migrate:his  显示执行的 up/down 历史

php ./bin/swoft migrate:history 显示执行的 up/down 历史

## Model类生成

php bin/swoft entity:create [tableName] 生成已表名命名的model类

## 开发文档
> **[Swoft](https://www.swoft.org/documents/v2/index.html)**
