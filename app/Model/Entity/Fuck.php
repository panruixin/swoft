<?php declare(strict_types=1);


namespace App\Model\Entity;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;


/**
 * 
 * Class Fuck
 *
 * @since 2.0
 *
 * @Entity(table="fuck")
 */
class Fuck extends Model
{
    /**
     * 
     * @Id()
     * @Column()
     *
     * @var int
     */
    private $id;

    /**
     * 
     *
     * @Column(name="create_id", prop="createId")
     *
     * @var int|null
     */
    private $createId;

    /**
     * 
     *
     * @Column()
     *
     * @var string|null
     */
    private $fucking;

    /**
     * 
     *
     * @Column(name="fucking_name", prop="fuckingName")
     *
     * @var string|null
     */
    private $fuckingName;


    /**
     * @param int $id
     *
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param int|null $createId
     *
     * @return self
     */
    public function setCreateId(?int $createId): self
    {
        $this->createId = $createId;

        return $this;
    }

    /**
     * @param string|null $fucking
     *
     * @return self
     */
    public function setFucking(?string $fucking): self
    {
        $this->fucking = $fucking;

        return $this;
    }

    /**
     * @param string|null $fuckingName
     *
     * @return self
     */
    public function setFuckingName(?string $fuckingName): self
    {
        $this->fuckingName = $fuckingName;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getCreateId(): ?int
    {
        return $this->createId;
    }

    /**
     * @return string|null
     */
    public function getFucking(): ?string
    {
        return $this->fucking;
    }

    /**
     * @return string|null
     */
    public function getFuckingName(): ?string
    {
        return $this->fuckingName;
    }

}
