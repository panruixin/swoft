<?php declare(strict_types=1);


namespace App\Model\Entity;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;


/**
 * 
 * Class User
 *
 * @since 2.0
 *
 * @Entity(table="user")
 */
class User extends Model
{
    /**
     * 
     * @Id()
     * @Column()
     *
     * @var int
     */
    private $id;

    /**
     * 
     *
     * @Column()
     *
     * @var string|null
     */
    private $name;

    /**
     * 
     *
     * @Column()
     *
     * @var int|null
     */
    private $age;

    /**
     * 
     *
     * @Column(hidden=true)
     *
     * @var string|null
     */
    private $password;

    /**
     * 
     *
     * @Column()
     *
     * @var string|null
     */
    private $userDesc;

    /**
     * 
     *
     * @Column()
     *
     * @var string|null
     */
    private $testJson;


    /**
     * @param int $id
     *
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string|null $name
     *
     * @return self
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param int|null $age
     *
     * @return self
     */
    public function setAge(?int $age): self
    {
        $this->age = $age;

        return $this;
    }

    /**
     * @param string|null $password
     *
     * @return self
     */
    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param string|null $userDesc
     *
     * @return self
     */
    public function setUserDesc(?string $userDesc): self
    {
        $this->userDesc = $userDesc;

        return $this;
    }

    /**
     * @param string|null $testJson
     *
     * @return self
     */
    public function setTestJson(?string $testJson): self
    {
        $this->testJson = $testJson;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return int|null
     */
    public function getAge(): ?int
    {
        return $this->age;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @return string|null
     */
    public function getUserDesc(): ?string
    {
        return $this->userDesc;
    }

    /**
     * @return string|null
     */
    public function getTestJson(): ?string
    {
        return $this->testJson;
    }

}
