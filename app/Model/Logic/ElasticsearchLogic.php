<?php


namespace App\Model\Logic;


use Elasticsearch\ClientBuilder;
use Swoft\Bean\Annotation\Mapping\Bean;
use Swoftx\Elasticsearch\CoroutineHandler;

/**
 * Class ElasticsearchLogic
 * @package App\Model\Logic
 * @Bean("ElasticsearchLogic")
 */
class ElasticsearchLogic
{
    private $es;

    /**
     * @return void
     */
    public function __constrcut(){
        $handle = CoroutineHandler::class([
            'timeout'=>3
        ]);
        $client = ClientBuilder::create()
            ->setHosts([env('ESHOST','192.168.1.56').":".env("ESPORT",'9200')])
            ->setHandler($handle)
            ->build();
        $this->es = $client;
    }

    /**
     * @param array $prarm
     * @return object
     */
    public function index(array $prarm) : object
    {
        $id = $prarm['id'];
        $desc = $prarm['desc'];
        $params = [
            'index' => 'test',
            'type' => 'movie',
            'id' => $id,
            'body' => ['name' => $desc]
        ];
        $res = $this->es->index($params);
        return $res;
    }

    public function search(array $param)
    {
        $id = $param['id'] ?? '';
        $size = $param['size'] ?? 10;
        $page = $param['page'] ? (intval($param['page'])-1)*$size : 0;
        $boost = $param['boost'] ?? 1;
        $keywords = $param['keywords'];

        $params = [
            'index'  => 'test',
            'type'   => 'movie',
            'body' => [
                'query' => [
                    'match' => []
                ]
            ],
        ];

        if($id){
            $params["body"]["query"]["match"]["id"]["query"] = $id;
            $params["body"]["query"]["match"]["id"]["boost"] = $boost;
        }else{
            $params["body"]["query"]["match"]["id"]["query"] = $keywords;
            $params["body"]["query"]["match"]["id"]["boost"] = $boost;
        }

        $res = $this->es->search($params);
        return $res;
    }

}
