<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link     https://swoft.org
 * @document https://swoft.org/docs
 * @contact  group@swoft.org
 * @license  https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Rpc\Service;

use App\Rpc\Lib\DemoInterface;
use Swoft\Rpc\Server\Annotation\Mapping\Service;

/**
 * Class UserService
 *
 * @since 2.0
 *
 * @Service(version="1.2")
 */
class DemoServiceV2 implements DemoInterface
{
    /**
     * @param int $id
     *
     * @return array
     */
    public function index(int $id): array
    {
        return [
            'name' => $id,
            'v'    => '1.2'
        ];
    }

}
