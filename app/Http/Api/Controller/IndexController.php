<?php
declare(strict_types=1);

namespace App\Http\Api\Controller;

use App\Model\Entity\User;
use Elasticsearch\ClientBuilder;
use Swoft\Bean\Annotation\Mapping\Inject;
use Swoft\Http\Message\Request;
use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use Swoft\Http\Server\Annotation\Mapping\Middleware;
use App\Http\Middleware\UsersMiddleware;
use Swoft\Http\Message\Response;
use Swoftx\Elasticsearch\CoroutineHandler;
use App\Model\Logic\ElasticsearchLogic;
use function context;

/**
 * Class IndexController
 * @Controller(prefix="/v1/api")
 */
class IndexController {
    /**
     * @Inject("ElasticsearchLogic")
     * @var ElasticsearchLogic
     */
    protected $elasticsearchLogic;

    /**
     * @RequestMapping("index")
     * @Middleware(UsersMiddleware::class)
     * @return array
     */
    public function index(Request $request) : Response{
        $req = $request->input('key');
        return context()->getResponse()->withContentType('application/json')->withData($req);
    }

    /**
     * @RequestMapping("set")
     * @return Response
     */
    public function set(Request $request): Response{
        $data = $request->input();
        $user = new User();

        $user->setName('Swoft');
        $user->setPassword('123456');
        $user->setAge(2);
        $user->setUserDesc('Great Framework');

        $user->save();
        $userId = $user->getId();
        var_dump($userId);
        return context()->getResponse()->withContentType('application/json')->withData(['result'=>$userId]);
    }

    /**
     * @RequestMapping("updatebyid")
     * @return string
     */
    public function updatebyid(Request $request) : Response{
        $id = $request->input('id');
        $values = [
            ['id' => $id, 'age' => 18],
        ];

        $result = User::batchUpdateByIds($values);
        return context()->getResponse()->withContentType('application/json')->withData(['result'=>$result]);
    }

    /**
     * @RequestMapping("suduupdate")
     * @return response
     */
    public function suduupdate(Request $request) : Response
    {
        $id = $request->input("id");
        $result = User::modifyById($id, ['age' => 2]);
        return context()->getResponse()->withContentType("application/json")->withData(['result'=>$result]);
    }

    /**
     * @RequestMapping("updateOrCreate")
     * @return response
     */
    public function updateOrCreate (Request $request) : Response
    {
        $id = $request->input("id");
        $result = User::updateOrCreate(['id' => $id], ['age' => 103, 'name' => 'PAN']);
        return context()->getResponse()->withContentType("application/json")->withData(['result'=>$result]);
    }

    /**
     * @RequestMapping("incrementage")
     * @return response
     */
    public function incrementage (Request $request) : Response
    {
        $id = $request->input("id");
        $result = User::find($id)->increment('age', 10);//加10
        $result2 = User::find($id)->increment('age', 1, ['name' => 'PAN']);//加2
        return context()->getResponse()->withContentType("application/json")->withData(['result1'=>$result,'result2'=>$result2]);
    }

    /**
     * @RequestMapping("orderfine")
     * @return response
     */
    public function orderfine (Request $request) : Response
    {
        $id = $request->input("id");
        $result = User::find($id)->increment('age', 10);//加10
        $result2 = User::find($id)->increment('age', 1, ['name' => 'PAN']);//加2
        return context()->getResponse()->withContentType("application/json")->withData(['result1'=>$result,'result2'=>$result2]);
    }

    /**
     * @RequestMapping("esput")
     * @return Response
     */
    public function esput(Request $request) : Response
    {
        $id = $request->input('id');
        $desc = $request->input('desc');

        $response = $this->elasticsearchLogic->index(['id'=>$id,'desc'=>$desc]);

        return context()->getResponse()->withContentType("application/json")->withData($response);
    }

    /**
     * @RequestMapping("esget")
     * @return Response
     */
    public function esget(Request $request) : Response
    {
        $id = $request->input('id');
        $handle = new CoroutineHandler([
            'timeout' => 3
        ]);

        $client = ClientBuilder::create()
            ->setHosts(['192.168.1.56:9200'])
            ->setHandler($handle)
            ->build();

        $param = [
            'index' => 'test',
            'type'  => 'movie',
            'id'    => $id,
            'client'=> [ 'ignore' => 404 ]
        ];

        return context()->getResponse()->withContentType("application/json")->withData($client->get($param));

    }

    /**
     * @RequestMapping("essearch")
     *
     * @return Response
     */
    public function essearch(Request $request) : Response
    {
        $keywords = $request->input("keywords");
        $page = $request->input("page",1);
        $size = $request->input("size",10);

        $res = $this->elasticsearchLogic->search(['keywords'=>$keywords,'page'=>$page,'size'=>$size]);

        return context()->getResponse()->withContentType("application/json")->withData($res);
    }
    /**
     * @RequestMapping("esboostsearch")
     * @return Response
     */
    public function esboostsearch(Request $request){
        $keywords = $request->input('keywords');
        $result = $this->elasticsearchLogic->search(['keywords'=>$keywords,'boost'=>2]);
        return context()->getResponse()->withContentType("application/json")->withData($result);
    }

    /**
     * @RequestMapping("analyzer")
     * @return Response
     */
    public function analyzer(Request $request){
        $keywords = $request->input("keywords");

        $res = $this->elasticsearchLogic->search(['keywords'=>$keywords]);

        return context()->getResponse()->withContentType("application/json")->withData($res);
    }
}




